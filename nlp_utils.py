emotions = [
    "Happy",
    "Satisfied",
    "Calm",
    "Proud",
    "Excited",
    "Frustrated",
    "Anxious",
    "Surprised",
    "Nostalgic",
    "Bored",
    "Sad",
    "Angry",
    "Confused",
    "Disgusted",
    "Afraid",
    "Ashamed",
    "Awkward",
    "Jealous",
]

topics = [
    "Family",
    "Work",
    "Food",
    "Sleep",
    "Friends",
    "Health",
    "Recreation",
    "God",
    "Love",
    "School",
    "Exercise",
]


def get_emotions(data, tolerance=0.3):
    result = []
    for i in range(len(emotions)):
        if data[i] > tolerance:
            result.append(emotions[i])
    return result


def get_topics(data, tolerance=0.3):
    result = []
    for i in range(len(topics)):
        if data[i] > tolerance:
            result.append(topics[i])
    return result
