from pydantic import BaseModel

class MentalDisorderModelRequest(BaseModel):
    kesedihan: int
    euphoria: int
    lelah: int
    gangguanTidur: int
    moodSwing: int
    pikiranBunuhDiri: int
    anoreksia: int
    menghormatiOtoritas: int
    memberikanPenjelasan: int
    responsAgresif: int
    tidakPeduli: int
    mudahGugup: int
    mengakuiKesalahan: int
    overthinking: int
    aktivitasSeksual: int
    mudahKonsentrasi: int
    optimis: int

class DepressionLevelModelRequest(BaseModel): #punya angga
    appetite: int
    interest: int
    fatigue: int
    worthlessness: int
    concentration: int
    agitation: int
    suicidalIdeation: int
    sleepDisturbance: int
    aggression: int
    panicAttacks: int
    hopelessness: int
    restlessness: int
    lowEnergy: int

class NlpModelRequest(BaseModel):
    answer: str