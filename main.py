from typing import Union
from model_requests import (
    NlpModelRequest,
    MentalDisorderModelRequest,
    DepressionLevelModelRequest,
)
from fastapi import FastAPI
import tensorflow as tf
import tensorflow_text as text
import pandas as pd
from google.cloud import storage
import os
from tensorflow.keras.models import load_model
import numpy as np
from nlp_utils import get_emotions, get_topics, get_emotions, get_topics
from depression_util import get_depression_label

app = FastAPI()

PROJECT_NAME = "mentalk"
CREDENTIALS = os.getenv(
    "GOOGLE_APPLICATION_CREDENTIALS"
)  # Change this to your service account key (read README.md for mor information)
bucket_name = "mentalk-model-bucket"


def download_model(model_name):
    model_path = f"models/{model_name}"
    if os.path.exists(model_path):
        print(f"Model {model_name} already downloaded.")
        return model_path

    client = storage.Client.from_service_account_json(CREDENTIALS)
    bucket = client.get_bucket(bucket_name)
    blobs = bucket.list_blobs(prefix=f"{model_name}/")

    os.makedirs(model_path, exist_ok=True)

    for blob in blobs:
        local_path = os.path.join("models", blob.name)
        os.makedirs(os.path.dirname(local_path), exist_ok=True)
        print(f"Downloading {blob.name} to {local_path}")
        blob.download_to_filename(local_path)

    return model_path


# Use the function to download the model
download_model("mental_health_diagnosis_model")
mental_health_model = tf.saved_model.load("models/mental_health_diagnosis_model")

download_model("depression_model")
depression_model = tf.saved_model.load("models/depression_model")

download_model("fullmodel_emotions")
download_model("fullmodel_topics")
emotions_model = tf.saved_model.load("models/fullmodel_emotions")
topics_model = tf.saved_model.load("models/fullmodel_topics")


@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.post("/model/mental_disorder")
def mental_disorder(req: MentalDisorderModelRequest):
    data = pd.DataFrame([dict(req)])
    input_data = data.astype(float).values

    prediction = mental_health_model.signatures["serving_default"](
        tf.constant(input_data, dtype=tf.float32)
    )

    softmax_predictions = prediction["output_0"].numpy().tolist()

    predicted_class = np.argmax(softmax_predictions)
    labels = ["Bipolar Type-1", "Bipolar Type-2", "Depression", "Normal"]
    predicted_label = labels[predicted_class]

    return {"predicted_label": predicted_label}


@app.post("/model/depression_level")
def depression_level(req: DepressionLevelModelRequest):
    data = pd.DataFrame([dict(req)])
    input_data = data.astype(float).values

    prediction = depression_model.signatures["serving_default"](
        tf.constant(input_data, dtype=tf.float32)
    )

    prediction_list = list(prediction.values())[0].numpy().tolist()

    predicted_value = prediction_list[0][0]
    predicted_label = get_depression_label(predicted_value)

    return {"result": predicted_label}


@app.post("/model/text_emotion")
def text_emotion(req: NlpModelRequest):
    synopsis = req.answer
    input_data = tf.constant([synopsis])

    emotion_prediction = emotions_model.signatures["serving_default"](input_data)
    emotion_prediction_list = (
        emotion_prediction[next(iter(emotion_prediction))].numpy().tolist()[0]
    )

    predicted_class = np.argmax(emotion_prediction_list)
    labels = ["anger", "fear", "joy", "love", "neutral", "sadness", "surprise"]
    predicted_label = labels[predicted_class]

    return {
        "emotion_result": predicted_label,
    }


@app.post("/model/wdyt_yesterday")
async def wdyt_yesterday(req: NlpModelRequest):
    synopsis = req.answer

    input_data = tf.constant([synopsis])

    emotion_prediction = emotions_model.signatures["serving_default"](input_data)
    topic_prediction = topics_model.signatures["serving_default"](input_data)

    emotion_prediction_list = (
        emotion_prediction[next(iter(emotion_prediction))].numpy().tolist()[0]
    )
    topic_prediction_list = (
        topic_prediction[next(iter(topic_prediction))].numpy().tolist()[0]
    )

    filtered_emotions = get_emotions(emotion_prediction_list)
    filtered_topics = get_topics(topic_prediction_list)

    return {
        "emotion_result": filtered_emotions,
        "topic_result": filtered_topics,
    }
