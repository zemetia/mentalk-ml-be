def get_depression_label(value):
    if 0 <= value < 0.25:
        return "no depression"
    elif 0.25 <= value < 0.5:
        return "mild"
    elif 0.5 <= value < 0.75:
        return "moderate"
    else:
        return "severe"
