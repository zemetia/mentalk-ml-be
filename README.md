# mentalk-ml-be

### Setting up a virtual environment

1. Navigate to your project directory:

    ```bash
    cd your_project_directory
    ```

2. Create a new virtual environment using Python's built-in `venv` module:

    ```bash
    python -m venv venv
    ```

3. Activate the virtual environment:

    ```bash
    .\venv\Scripts\activate
    ```

4. Install the required Python packages:

    ```bash
    pip install fastapi uvicorn
    ```

### Setting up GCP credentials

1. Set the `GOOGLE_APPLICATION_CREDENTIALS` environment variable to the path of your GCP credentials JSON file:

    ```bash
    $env:GOOGLE_APPLICATION_CREDENTIALS="C:\path\to\your\credentials.json"
    ```

    Replace `C:\path\to\your\credentials.json` with the actual path to your JSON file.

2. Verify that the environment variable has been set correctly:

    ```bash
    echo $env:GOOGLE_APPLICATION_CREDENTIALS
    ```

    This should output the path that you set in the previous step.

### Running the program

1. Once you have set up your virtual environment and installed the necessary packages, you can run your FastAPI application using uvicorn. Navigate to the directory containing your main application file 

    ```bash
    cd path_to_your_application_directory
    ```

2. Run the application with uvicorn:

    ```bash
    uvicorn main:app --reload
    ```